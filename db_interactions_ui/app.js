
document.getElementById('submit').addEventListener('click',function(event){
   
   var addWorkout = document.getElementById("addNewWorkout");
   var req = new XMLHttpRequest();
   var qString = "/insert";
   var parameters = "exercise="+addWorkout.elements.exercise.value+
                  "&reps="+addWorkout.elements.reps.value+
                  "&weight="+addWorkout.elements.weight.value+
                  "&date="+addWorkout.elements.date.value;
 
   if(addWorkout.elements.measurement.checked){
      parameters+="&measurement=1";
   }
   else{
      parameters+="&measurement=0";
   }

   // open the request
   req.open("GET", qString +"?"+parameters, true);
   req.setRequestHeader('Content-Type','application/x-www-form-urlencoded');

   req.addEventListener('load', function(){
      if(req.status >= 200 && req.status < 400){

         var response = JSON.parse(req.responseText);
         var id = response.inserted;
         var table = document.getElementById("dataTable");
         var row = table.insertRow(-1);
         var nameEx = document.createElement('td');
         nameEx.textContent = addWorkout.elements.exercise.value;
         row.appendChild(nameEx);
         var repEx = document.createElement('td');
         repEx.textContent = addWorkout.elements.reps.value;
         row.appendChild(repEx);

         // create a cell for the weight
         var weightEx = document.createElement('td');
         weightEx.textContent = addWorkout.elements.weight.value;
         row.appendChild(weightEx);

         // if they checked the LBS checkbox, then set the text content to LBS
         // otherwise set it to KG, then create the cell
         var measureEx = document.createElement('td');
         if(addWorkout.elements.measurement.checked){
            measureEx.textContent = "LBS"
         }
         else{
            measureEx.textContent = "KG"
         }
         row.appendChild(measureEx);

         // create a cell for the date
         var dateEx = document.createElement('td');
         dateEx.textContent = addWorkout.elements.date.value;
         row.appendChild(dateEx);

         // These are a bit more involved. 
         // since we are redirecting to another page, 
         // add a link and then inside add a button so that the 
         // button will cause the redirect
         // to the update page with the id as a parameter
         var updateCell = document.createElement('td');
         var updateLink = document.createElement('a');
         updateLink.setAttribute('href','/update?id='+id);
         var updateButton = document.createElement('input');
         updateButton.setAttribute('type','button');
         updateButton.setAttribute('value','Update');
         updateLink.appendChild(updateButton);
         updateCell.appendChild(updateLink);
         row.appendChild(updateCell);

         // we want a delete button that calls the deleteRow function
         // so create a cell that contains two inputs, a hidden one with id
         // delete+{{this.id}} and one that calls the deleteRow funciton with
         // {{this.id}}
         var deleteCell = document.createElement('td');
         var deleteButton = document.createElement('input');
         deleteButton.setAttribute('type','button');
         deleteButton.setAttribute('name','delete');
         deleteButton.setAttribute('value','Delete');
         deleteButton.setAttribute('onClick', 'deleteRow("dataTable",'+id+')');
         var deleteHidden = document.createElement('input');
         deleteHidden.setAttribute('type','hidden');
         deleteHidden.setAttribute('id', 'delete'+id);
         deleteCell.appendChild(deleteButton);
         deleteCell.appendChild(deleteHidden);
         row.appendChild(deleteCell);


      }
      else {
         console.log('there was an error');
      }
   });
   
   // send request
   req.send(qString +"?"+parameters);
   event.preventDefault();
});

function deleteRow(tableId, id){
   // get the table and row count
   var table = document.getElementById(tableId);
   var rowCount = table.rows.length;

   // create a matching id for the hidden id
   var deleteString = "delete"+id;

   // loop through the rows (not including the header) to find the 
   // row with the delete cell that has a hidden input that contains
   // the id="delete{{this.id}}"
   for(var i = 1; i < rowCount; i++){
      var row = table.rows[i];
      var dataCells = row.getElementsByTagName("td");
      // console.log(dataCells);
      var deleteCell = dataCells[dataCells.length -1];
      // console.log(deleteCell);
      if(deleteCell.children[1].id === deleteString){
         // delete that row
         table.deleteRow(i);
      }

   }

   // setup and send a delete request to the server so it can delete the entry
   // from the database
   var req = new XMLHttpRequest();
   var qString = "/delete";

   req.open("GET", qString+"?id="+id, true);

   req.addEventListener("load",function(){
      if(req.status >= 200 && req.status < 400){
         console.log('delete request sent');
      } else {
          console.log('there was an error');
      }
   });

   req.send(qString+"?id="+id );

}