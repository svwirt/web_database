var express = require("express");
var bodyParser = require("body-parser");       
var app = express();
var handlebars = require("express-handlebars").create({defaultLayout: "main"});

app.engine("handlebars", handlebars.engine);
app.set("view engine", "handlebars");
app.set("port", 7479);
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get("/", function(req, res)  
{
  var qParams = [];
  for (var p in req.query) 
    qParams.push({"name": p, "value": req.query[p]});  
  var show = {};
  show.dataList = qParams;
  res.render("get", show);                                
});

app.post("/", function(req, res)                        
{
  var qParams = [];
  for (var p in req.query)
    qParams.push({"name": p, "value": req.query[p]});   
  var otherPs = [];
  for (var b in req.body) 
    otherPs.push({"name": b, "value": req.body[b]});
  var show = {};
  show.queryList = qParams;
  show.bodyList = otherPs;
  res.render("post", show);                                              
});

app.use(function(req, res){
  res.status(404);
  res.render("404");
});

app.use(function(err, req, res, next){
  console.log(err.stack);
  res.status(500);
  res.render("500");
});

app.listen(app.get("port"), function(){
  
});