function MakeTable(){
    var bod = document.body,
    myTable = document.createElement('table');
 
    //styling table and adding cells 
    myTable.style.width = '500px';
    myTable.style.height = '500px';
    myTable.style.border = '2px solid black';
    myTable.setAttribute("id", "myTable");
    
    // make the rows/collums
    var rows = myTable.insertRow();

    var myHeader=document.createElement("TH");
    myHeader.appendChild(document.createTextNode("Header "+1))
    myHeader.style.border = '2px solid black';
    rows.appendChild(myHeader);

    var myHeader=document.createElement("TH");
    myHeader.appendChild(document.createTextNode("Header "+ 2))
    myHeader.style.border = '2px solid black';
    rows.appendChild(myHeader);

    var myHeader=document.createElement("TH");
    myHeader.appendChild(document.createTextNode("Header "+ 3))
    myHeader.style.border = '2px solid black';
    rows.appendChild(myHeader);

    var myHeader=document.createElement("TH");
    myHeader.appendChild(document.createTextNode("Header "+ 4))
    myHeader.style.border = '2px solid black';
    rows.appendChild(myHeader);


   for(var i = 1; i < 4; i++){
        var rows = myTable.insertRow();
        for(var j = 1; j < 5; j++){
            var cells = rows.insertCell();
            cells.appendChild(document.createTextNode(j +', '+ i));
            cells.style.border = '1px dotted black';
        }
    }
   bod.appendChild(myTable);

   // Up button
    var myElement = document.createElement("input");
    myElement.type = "button";
    myElement.value = "Up";
    myElement.onclick = function() {
       var currentPos=table.rows[rPosition].cells[cPosition];
       if(rPosition != 1){
          table.rows[rPosition].cells[cPosition].style.border='1px dotted black';
          rPosition=rPosition-1;
          currentPos=table.rows[rPosition].cells[cPosition];
          currentPos.style.border='2px solid black';
       }
       currentPos = currentPos;
        
    };
    bod.appendChild(myElement);

    // Down button
    var myElement1 = document.createElement("input");
    myElement1.type = "button";
    myElement1.value = "Down";
    myElement1.onclick = function() {
       var currentPos=table.rows[rPosition].cells[cPosition];
       if(rPosition != 3){
           table.rows[rPosition].cells[cPosition].style.border='1px dotted black';
            rPosition=rPosition+1;
            currentPos=table.rows[rPosition].cells[cPosition];
            currentPos.style.border='2px solid black';
       }
        currentPos = currentPos;
        
    };
    bod.appendChild(myElement1);


     // Left Button
    var myElement2 = document.createElement("input");
    myElement2.type = "button";
    myElement2.value = "Left";
    myElement2.onclick = function() {
       var currentPos=table.rows[rPosition].cells[cPosition];
       if(cPosition != 0){
           table.rows[rPosition].cells[cPosition].style.border='1px dotted black';
           cPosition=cPosition-1;
           currentPos=table.rows[rPosition].cells[cPosition];
           currentPos.style.border='2px solid black';
        }
        currentPos = currentPos;
        
    };
    bod.appendChild(myElement2);

    // Right button
    var myElement3 = document.createElement("input");
    myElement3.type = "button";
    myElement3.value = "Right";
    myElement3.onclick = function() {
       var currentPos=table.rows[rPosition].cells[cPosition];
       if(cPosition != 3){
           table.rows[rPosition].cells[cPosition].style.border='1px dotted black';
           cPosition=cPosition+1;
           currentPos=table.rows[rPosition].cells[cPosition];
           currentPos.style.border='2px solid black';
        }
        currentPos = currentPos;
 
    };
    bod.appendChild(myElement3);

   // Mark cell with yellow
    var myElement4 = document.createElement("input");
    myElement4.type = "button";
    myElement4.value = "Markcell";
    myElement4.onclick = function() {
        table.rows[rPosition].cells[cPosition].style.backgroundColor= 'yellow';
    };
    bod.appendChild(myElement4);

    //outline current cell
    var table = document.getElementById("myTable");
    var rPosition = 1;
    var cPosition = 0;
    table.rows[rPosition].cells[cPosition].style.border='2px solid black';
}

window.onload=MakeTable();